import 'package:get/get.dart';
import 'package:maha_flutter_test/_core/constants/app_page.dart';

class SplashController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    initSplash();
  }

  void initSplash() async {
    await Future.delayed(const Duration(seconds: 2));
    Get.offAllNamed(AppPage.homeScreen);
  }
}
