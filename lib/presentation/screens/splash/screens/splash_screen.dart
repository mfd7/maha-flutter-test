import 'package:flutter/material.dart';
import 'package:maha_flutter_test/_core/constants/app_constant.dart';
import 'package:maha_flutter_test/presentation/_core/app_color.dart';
import 'package:maha_flutter_test/presentation/_core/app_text_style.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            color: AppColor.primary500,
          ),
          Positioned(
            top: 632,
            left: -160,
            child: SizedBox(
              width: 340,
              height: 340,
              child: Stack(
                children: [
                  Positioned(
                    top: 0.5,
                    child: Container(
                      width: 340,
                      height: 340,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: AppColor.neutral1.withOpacity(0.08),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 42.5,
                    left: 42.5,
                    child: Container(
                      width: 255,
                      height: 255,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: AppColor.neutral1.withOpacity(0.08),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 85,
                    left: 85,
                    child: Container(
                      width: 170,
                      height: 170,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: AppColor.neutral1.withOpacity(0.08),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: -100,
            left: 235,
            child: SizedBox(
              width: 340,
              height: 340,
              child: Stack(
                children: [
                  Positioned(
                    top: 0.5,
                    child: Container(
                      width: 340,
                      height: 340,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: AppColor.neutral1.withOpacity(0.08),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 42.5,
                    left: 42.5,
                    child: Container(
                      width: 255,
                      height: 255,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: AppColor.neutral1.withOpacity(0.08),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 85,
                    left: 85,
                    child: Container(
                      width: 170,
                      height: 170,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: AppColor.neutral1.withOpacity(0.08),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned.fill(
            child: SafeArea(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    clipBehavior: Clip.antiAlias,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Container(),
                  ),
                  const SizedBox(
                    height: AppConstant.kDefaultPadding / 2,
                  ),
                  Text(
                    'Maha Flutter Test',
                    style: AppTextStyle.titleQuotes
                        .copyWith(color: AppColor.neutral1),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
