import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:maha_flutter_test/_core/constants/app_page.dart';
import 'package:maha_flutter_test/domain/_core/entities/meta.dart';
import 'package:maha_flutter_test/domain/user/entities/user_entity.dart';
import 'package:maha_flutter_test/domain/user/repositories/user_repository.dart';
import 'package:maha_flutter_test/presentation/_core/app_color.dart';
import 'package:maha_flutter_test/presentation/_core/widgets/custom_bottom_sheet.dart';
import 'package:maha_flutter_test/presentation/_core/widgets/custom_dialog.dart';
import 'package:maha_flutter_test/presentation/screens/home/widgets/user_bottom_sheet.dart';

class HomeController extends GetxController {
  final UserRepository _userRepository;

  HomeController(this._userRepository);

  final users = <UserEntity>[].obs;
  final meta = const Meta().obs;
  final isLoading = false.obs;
  final isOverlayLoading = false.obs;
  final scrollController = ScrollController();

  @override
  void onInit() {
    super.onInit();
    scrollController.addListener(_fetchMoreUsers);
    retrieveUsers();
  }

  @override
  void onClose() {
    scrollController.removeListener(_fetchMoreUsers);
    super.onClose();
  }

  Future<void> retrieveUsers([bool isLoadMore = false]) async {
    if (!isLoadMore) {
      isLoading(true);
    }
    final response = await _userRepository.retrieveUserList(meta.value.page);
    response.fold((failure) {
      Get.snackbar('Terjadi Kesalahan', 'Silahkan Cek Internet Anda',
          backgroundColor: AppColor.dangerNormal);
    }, (data) {
      if (isLoadMore) {
        users.addAll(data.users);
        users.refresh();
      } else {
        users.value = data.users;
      }
      meta.value = data.meta;
    });
    isLoading(false);
  }

  Future<void> deleteUser(int id) async {
    isOverlayLoading(true);
    final response = await _userRepository.deleteUser(id);
    response.fold((failure) {
      Get.snackbar('Terjadi Kesalahan', 'Silahkan Cek Internet Anda',
          backgroundColor: AppColor.dangerNormal);
    }, (data) {
      Get.snackbar('Success', data, backgroundColor: AppColor.successNormal);
    });
    isOverlayLoading(false);
  }

  _fetchMoreUsers() {
    if (scrollController.position.pixels ==
            scrollController.position.maxScrollExtent &&
        meta.value.page < meta.value.totalPages) {
      meta.value = meta.value.copyWith(page: meta.value.page + 1);
      retrieveUsers(true);
    }
  }

  onCardTapped(UserEntity user) async {
    final result = await Get.bottomSheet(
      isScrollControlled: true,
      CustomBottomSheet(
        title: user.name,
        child: UserBottomSheet(
          user: user,
        ),
      ),
    );
    if (result != null) {
      if (result) {
        goToEditAdd(false, user: user);
      } else {
        final sure = await Get.dialog(
          const CustomDialog(
            title: 'Delete User',
            text: 'Are you sure you want to delete this user?',
            falseText: 'Cancel',
            trueText: 'Yes',
          ),
        );
        if (sure) {
          deleteUser(user.id);
        }
      }
    }
  }

  goToEditAdd(bool isAdd, {UserEntity? user}) async {
    final result = await Get.toNamed(AppPage.editAddScreen,
        arguments: {'isAdd': isAdd, 'user': user});
    if (result != null) {
      Get.snackbar('Success', result, backgroundColor: AppColor.successNormal);
    }
  }

  Future<void> onRefresh() async {
    users.clear();
    meta.value = const Meta();
    retrieveUsers();
  }
}
