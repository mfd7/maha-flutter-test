import 'package:get/get.dart';
import 'package:maha_flutter_test/presentation/screens/home/controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(HomeController(Get.find()));
  }
}
