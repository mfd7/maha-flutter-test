import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maha_flutter_test/_core/constants/app_constant.dart';
import 'package:maha_flutter_test/presentation/_core/app_color.dart';
import 'package:maha_flutter_test/presentation/_core/app_text_style.dart';
import 'package:maha_flutter_test/presentation/_core/widgets/custom_shimmer.dart';
import 'package:maha_flutter_test/presentation/screens/home/controllers/home_controller.dart';
import 'package:maha_flutter_test/presentation/screens/home/widgets/user_card.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({super.key});

  final _controller = Get.find<HomeController>();
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        appBar: AppBar(
          backgroundColor: AppColor.primary500,
          title: Text(
            AppConstant.kAppName,
            style: AppTextStyle.titleLarge.copyWith(color: AppColor.neutral1),
          ),
        ),
        body: Obx(
          () => Stack(
            children: [
              RefreshIndicator(
                onRefresh: _controller.onRefresh,
                color: AppColor.primary500,
                child: Obx(
                  () => ListView.separated(
                    controller: _controller.scrollController,
                    padding: const EdgeInsets.symmetric(
                        vertical: AppConstant.kDefaultPadding),
                    itemCount: _controller.isLoading.value
                        ? 10
                        : _controller.users.length + 1,
                    itemBuilder: (context, index) {
                      if (index == _controller.users.length) {
                        if (_controller.meta.value.page ==
                            _controller.meta.value.totalPages) {
                          return Container();
                        }
                        return const Center(
                          child: CircularProgressIndicator(
                            color: AppColor.primary500,
                          ),
                        );
                      }
                      if (_controller.isLoading.value) {
                        return CustomShimmer(
                          child: UserCard(
                            name: 'item.name',
                            email: 'item.email',
                            imageUrl: 'item.imageUrl',
                            onTap: () {},
                          ),
                        );
                      }
                      final item = _controller.users.elementAt(index);
                      return UserCard(
                        name: item.name,
                        email: item.email,
                        imageUrl: item.imageUrl,
                        onTap: () => _controller.onCardTapped(item),
                      );
                    },
                    separatorBuilder: (_, __) {
                      return const SizedBox(
                        height: AppConstant.kDefaultPadding,
                      );
                    },
                  ),
                ),
              ),
              if (_controller.isOverlayLoading.value) ...[
                Container(
                  color: AppColor.neutral13.withOpacity(0.6),
                  child: const Center(
                    child: CircularProgressIndicator(
                      color: AppColor.primary500,
                    ),
                  ),
                )
              ]
            ],
          ),
        ),
        floatingActionButton: _controller.isOverlayLoading.value
            ? null
            : FloatingActionButton(
                onPressed: () => _controller.goToEditAdd(true),
                backgroundColor: AppColor.secondaryNormal,
                child: const Icon(
                  Icons.add,
                  color: AppColor.primary500,
                ),
              ),
      ),
    );
  }
}
