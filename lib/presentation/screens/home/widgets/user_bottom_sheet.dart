import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maha_flutter_test/_core/constants/app_constant.dart';
import 'package:maha_flutter_test/domain/user/entities/user_entity.dart';
import 'package:maha_flutter_test/presentation/_core/app_text_style.dart';
import 'package:maha_flutter_test/presentation/_core/widgets/custom_button.dart';

class UserBottomSheet extends StatelessWidget {
  const UserBottomSheet({
    super.key,
    required this.user,
  });
  final UserEntity user;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          user.email,
          style: AppTextStyle.bodyLarge,
        ),
        const SizedBox(
          height: AppConstant.kDefaultPadding,
        ),
        CachedNetworkImage(
          fit: BoxFit.cover,
          imageUrl: user.imageUrl,
          width: double.infinity,
        ),
        const SizedBox(
          height: AppConstant.kDefaultPadding * 2,
        ),
        Row(
          children: [
            Expanded(
              child: CustomButton.outlinedDangerMedium(
                onTap: () => Get.back(result: false),
                text: 'DELETE',
              ),
            ),
            const SizedBox(
              width: AppConstant.kDefaultPadding / 2,
            ),
            Expanded(
              child: CustomButton.outlinedPrimaryMedium(
                onTap: () => Get.back(result: true),
                text: 'EDIT',
              ),
            )
          ],
        )
      ],
    );
  }
}
