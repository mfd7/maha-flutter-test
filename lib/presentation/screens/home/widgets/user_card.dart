import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:maha_flutter_test/_core/constants/app_constant.dart';
import 'package:maha_flutter_test/presentation/_core/app_color.dart';
import 'package:maha_flutter_test/presentation/_core/app_text_style.dart';

class UserCard extends StatelessWidget {
  const UserCard({
    super.key,
    required this.name,
    required this.email,
    required this.imageUrl,
    required this.onTap,
  });

  final String name;
  final String email;
  final String imageUrl;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: AppConstant.kDefaultPadding / 2),
      child: InkWell(
        onTap: onTap,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Ink(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: AppColor.neutral1,
              boxShadow: [
                BoxShadow(
                  color: AppColor.neutral13.withOpacity(0.06),
                  offset: const Offset(2, 2),
                  blurRadius: 12,
                )
              ],
            ),
            child: Row(
              children: [
                Container(
                  clipBehavior: Clip.antiAlias,
                  decoration: const BoxDecoration(),
                  child: CachedNetworkImage(
                    fit: BoxFit.cover,
                    height: 100,
                    width: 100,
                    imageUrl: imageUrl,
                    placeholder: (context, val) {
                      return Container(
                        height: 100,
                        width: 100,
                        color: AppColor.neutral5,
                      );
                    },
                    errorWidget: (context, _, __) {
                      return Container(
                        height: 100,
                        width: 100,
                        color: AppColor.neutral5,
                      );
                    },
                  ),
                ),
                const SizedBox(
                  width: AppConstant.kDefaultPadding / 2,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        name,
                        style: AppTextStyle.titleMedium,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        email,
                        style: AppTextStyle.subHeading,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
