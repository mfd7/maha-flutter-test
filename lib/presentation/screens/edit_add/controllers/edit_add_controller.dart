import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:maha_flutter_test/domain/user/entities/user_entity.dart';
import 'package:maha_flutter_test/domain/user/repositories/user_repository.dart';
import 'package:maha_flutter_test/presentation/_core/app_color.dart';

class EditAddController extends GetxController {
  final UserRepository _userRepository;

  EditAddController(this._userRepository);

  final nameController = TextEditingController();
  final jobController = TextEditingController();
  final nameFocusNode = FocusNode().obs;
  final jobFocusNode = FocusNode().obs;
  final isValid = false.obs;
  final isNameValid = true.obs;
  final isJobValid = true.obs;
  final isLoading = false.obs;
  final isAdd = true.obs;
  final user = UserEntity().obs;

  @override
  void onInit() {
    super.onInit();
    getArguments();
    nameFocusNode.value.addListener(_onFocusChanged);
    jobFocusNode.value.addListener(_onFocusChanged);
  }

  @override
  void onClose() {
    nameFocusNode.value.removeListener(_onFocusChanged);
    jobFocusNode.value.removeListener(_onFocusChanged);
    super.onClose();
  }

  getArguments() {
    isAdd(Get.arguments['isAdd']);
    if (isAdd.isFalse) {
      user.value = Get.arguments['user'];
      nameController.text = user.value.name;
    }
  }

  Future<void> submitUser() async {
    isLoading(true);
    final response = await _userRepository.submitUser(
        nameController.text, jobController.text);
    response.fold((failure) {
      Get.snackbar('Terjadi Kesalahan', 'Silahkan Cek Internet Anda',
          backgroundColor: AppColor.dangerNormal);
    }, (data) {
      Get.back(result: data);
    });
    isLoading(false);
  }

  Future<void> updateUser() async {
    isLoading(true);
    final response = await _userRepository.updateUser(
        user.value.id, nameController.text, jobController.text);
    response.fold((failure) {
      Get.snackbar('Terjadi Kesalahan', 'Silahkan Cek Internet Anda',
          backgroundColor: AppColor.dangerNormal);
    }, (data) {
      Get.back(result: data);
    });
    isLoading(true);
  }

  checkNameValidity() {
    if (nameController.text.isEmpty) {
      isNameValid(false);
    } else {
      isNameValid(true);
    }
    checkValidity();
  }

  checkJobValidity() {
    if (jobController.text.isEmpty) {
      isJobValid(false);
    } else {
      isJobValid(true);
    }
    checkValidity();
  }

  checkValidity() {
    if (nameController.text.isEmpty || jobController.text.isEmpty) {
      isValid(false);
    } else {
      isValid(true);
    }
  }

  onSave() {
    if (isAdd.value) {
      submitUser();
    } else {
      updateUser();
    }
  }

  _onFocusChanged() {
    nameFocusNode.refresh();
    jobFocusNode.refresh();
  }
}
