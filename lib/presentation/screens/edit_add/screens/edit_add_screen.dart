import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maha_flutter_test/_core/constants/app_constant.dart';
import 'package:maha_flutter_test/presentation/_core/app_color.dart';
import 'package:maha_flutter_test/presentation/_core/app_text_style.dart';
import 'package:maha_flutter_test/presentation/_core/widgets/custom_button.dart';
import 'package:maha_flutter_test/presentation/_core/widgets/custom_text_field.dart';
import 'package:maha_flutter_test/presentation/screens/edit_add/controllers/edit_add_controller.dart';

class EditAddScreen extends StatelessWidget {
  EditAddScreen({super.key});
  final _controller = Get.find<EditAddController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: AppColor.neutral1),
            onPressed: () => Get.back(),
          ),
          backgroundColor: AppColor.primary500,
          title: Obx(
            () => Text(
              _controller.isAdd.value ? 'Add new user' : 'Edit user',
              style: AppTextStyle.titleLarge.copyWith(color: AppColor.neutral1),
            ),
          ),
        ),
        body: Obx(
          () => Stack(
            children: [
              Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      padding: const EdgeInsets.symmetric(
                          horizontal: AppConstant.kDefaultPadding / 4 * 5,
                          vertical: AppConstant.kDefaultPadding),
                      child: Column(
                        children: [
                          Obx(
                            () => CustomTextField(
                              inputTitle: 'Name',
                              controller: _controller.nameController,
                              focusNode: _controller.nameFocusNode.value,
                              isFocus: _controller.nameFocusNode.value.hasFocus,
                              isError: _controller.isNameValid.isFalse,
                              errorMessage: "Name can't be empty",
                              onChanged: (val) {
                                _controller.checkNameValidity();
                              },
                            ),
                          ),
                          const SizedBox(
                            height: AppConstant.kDefaultPadding,
                          ),
                          Obx(
                            () => CustomTextField(
                              inputTitle: 'Job',
                              controller: _controller.jobController,
                              focusNode: _controller.jobFocusNode.value,
                              isFocus: _controller.jobFocusNode.value.hasFocus,
                              isError: _controller.isJobValid.isFalse,
                              errorMessage: "job can't be empty",
                              onChanged: (val) {
                                _controller.checkJobValidity();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: AppConstant.kDefaultPadding,
                        horizontal: AppConstant.kDefaultPadding / 4 * 5),
                    child: SizedBox(
                      width: double.infinity,
                      child: Obx(
                        () => CustomButton.solidPrimaryLarge(
                          onTap: _controller.onSave,
                          isActive: _controller.isValid.value,
                          text: 'SAVE',
                          textColor: AppColor.primary500,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              if (_controller.isLoading.value) ...[
                Container(
                  color: AppColor.neutral13.withOpacity(0.6),
                  child: const Center(
                    child: CircularProgressIndicator(
                      color: AppColor.primary500,
                    ),
                  ),
                )
              ]
            ],
          ),
        ));
  }
}
