import 'package:get/get.dart';
import 'package:maha_flutter_test/presentation/screens/edit_add/controllers/edit_add_controller.dart';

class EditAddBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(EditAddController(Get.find()));
  }
}
