import 'package:flutter/material.dart';
import 'package:maha_flutter_test/presentation/_core/app_color.dart';
import 'package:shimmer/shimmer.dart';

class CustomShimmer extends StatelessWidget {
  final Widget child;
  const CustomShimmer({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColor.neutral5,
      highlightColor: AppColor.neutral1,
      child: child,
    );
  }
}

class TextPlaceHolder extends StatelessWidget {
  const TextPlaceHolder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 12,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: AppColor.neutral1,
      ),
    );
  }
}
