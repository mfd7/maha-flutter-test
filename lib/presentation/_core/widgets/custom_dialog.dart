import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maha_flutter_test/_core/constants/app_constant.dart';
import 'package:maha_flutter_test/presentation/_core/app_color.dart';
import 'package:maha_flutter_test/presentation/_core/app_text_style.dart';
import 'package:maha_flutter_test/presentation/_core/widgets/custom_button.dart';

class CustomDialog extends StatelessWidget {
  final String title;
  final String text;
  final bool twoOptions;
  final String falseText;
  final String trueText;
  const CustomDialog({
    super.key,
    required this.title,
    required this.text,
    this.twoOptions = true,
    required this.falseText,
    required this.trueText,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: const EdgeInsets.all(AppConstant.kDefaultPadding / 8 * 10),
            margin: const EdgeInsets.symmetric(
                horizontal: AppConstant.kDefaultPadding / 8 * 10),
            decoration: BoxDecoration(
              color: AppColor.neutral1,
              borderRadius: BorderRadius.circular(12),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: AppTextStyle.titleLarge.copyWith(
                    color: AppColor.neutral13,
                    decoration: TextDecoration.none,
                  ),
                ),
                const SizedBox(
                  height: AppConstant.kDefaultPadding / 2,
                ),
                Text(
                  text,
                  style: AppTextStyle.bodyLarge.copyWith(
                    color: AppColor.neutral7,
                    decoration: TextDecoration.none,
                  ),
                ),
                const SizedBox(
                  height: AppConstant.kDefaultPadding,
                ),
                if (twoOptions) ...[
                  Row(
                    children: [
                      Expanded(
                        child: CustomButton.outlinedPrimaryLarge(
                          onTap: () => Get.back(result: false),
                          text: falseText,
                        ),
                      ),
                      const SizedBox(
                        width: AppConstant.kDefaultPadding,
                      ),
                      Expanded(
                        child: CustomButton.flatDangerLarge(
                          onTap: () => Get.back(result: true),
                          text: trueText,
                          textColor: AppColor.dangerNormal,
                          color: AppColor.dangerBase,
                        ),
                      ),
                    ],
                  )
                ] else ...[
                  Align(
                    alignment: Alignment.centerRight,
                    child: CustomButton.textPrimaryLarge(
                      onTap: () => Get.back(result: false),
                      text: falseText,
                    ),
                  )
                ]
              ],
            ),
          ),
        ],
      ),
    );
  }
}
