import 'package:flutter/material.dart';
import 'package:maha_flutter_test/_core/constants/app_constant.dart';
import 'package:maha_flutter_test/presentation/_core/app_color.dart';
import 'package:maha_flutter_test/presentation/_core/app_text_style.dart';

class CustomBottomSheet extends StatelessWidget {
  final Widget child;
  final String title;
  final Widget? bottomNavigationBar;
  final bool usePadding;
  const CustomBottomSheet({
    super.key,
    required this.child,
    required this.title,
    this.bottomNavigationBar,
    this.usePadding = true,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: const BoxDecoration(
          color: AppColor.neutral1,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24),
            topRight: Radius.circular(24),
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              margin: EdgeInsets.zero,
              padding: const EdgeInsets.only(
                  top: AppConstant.kDefaultPadding,
                  right: AppConstant.kDefaultPadding / 8 * 10,
                  left: AppConstant.kDefaultPadding / 8 * 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      width: 32,
                      height: 4,
                      decoration: BoxDecoration(
                        color: AppColor.neutral6,
                        borderRadius: BorderRadius.circular(100),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: AppConstant.kDefaultPadding / 2 * 3,
                  ),
                  Text(
                    title,
                    style: AppTextStyle.titleMedium
                        .copyWith(color: AppColor.neutral13),
                  ),
                ],
              ),
            ),
            Flexible(
              child: SingleChildScrollView(
                padding: EdgeInsets.zero,
                child: Container(
                  margin: EdgeInsets.zero,
                  padding: usePadding
                      ? const EdgeInsets.only(
                          bottom: AppConstant.kDefaultPadding,
                          left: AppConstant.kDefaultPadding / 8 * 10,
                          right: AppConstant.kDefaultPadding / 8 * 10)
                      : null,
                  width: double.infinity,
                  color: AppColor.neutral1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: AppConstant.kDefaultPadding / 2,
                      ),
                      child
                    ],
                  ),
                ),
              ),
            ),
            if (bottomNavigationBar != null) ...[
              Align(
                  alignment: Alignment.bottomCenter,
                  child: bottomNavigationBar!)
            ],
          ],
        ),
      ),
    );
  }
}
