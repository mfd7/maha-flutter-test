import 'package:dartz/dartz.dart';
import 'package:maha_flutter_test/domain/_core/app_failures.dart';
import 'package:maha_flutter_test/domain/user/entities/user_list_entity.dart';

abstract class UserRepository {
  Future<Either<AppFailure, UserListEntity>> retrieveUserList(int page);
  Future<Either<AppFailure, String>> submitUser(String name, String job);
  Future<Either<AppFailure, String>> deleteUser(int id);
  Future<Either<AppFailure, String>> updateUser(
      int id, String name, String job);
}
