class UserEntity {
  final int id;
  final String imageUrl;
  final String name;
  final String email;

  UserEntity(
      {this.id = 0, this.imageUrl = '', this.name = '', this.email = ''});
}
