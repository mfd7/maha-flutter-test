import 'package:maha_flutter_test/domain/_core/entities/meta.dart';
import 'package:maha_flutter_test/domain/user/entities/user_entity.dart';

class UserListEntity {
  final Meta meta;
  final List<UserEntity> users;

  UserListEntity({this.meta = const Meta(), this.users = const []});
}
