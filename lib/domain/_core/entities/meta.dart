class Meta {
  final int page;
  final int perPage;
  final int total;
  final int totalPages;

  const Meta(
      {this.page = 1, this.perPage = 1, this.total = 1, this.totalPages = 1});

  Meta copyWith({
    int? page,
    int? perPage,
    int? total,
    int? totalPages,
  }) {
    return Meta(
      page: page ?? this.page,
      perPage: perPage ?? this.perPage,
      total: total ?? this.total,
      totalPages: totalPages ?? this.totalPages,
    );
  }
}
