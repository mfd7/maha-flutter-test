import 'package:alice_get_connect/alice_get_connect.dart';
import 'package:get/get.dart';
import 'package:maha_flutter_test/_core/constants/app_flavor.dart';
import 'package:maha_flutter_test/data/_core/api_base_helper.dart';
import 'package:maha_flutter_test/data/_core/interceptors/logger_interceptor.dart';
import 'package:maha_flutter_test/data/user/data_user_binding.dart';

class DataBinding extends Bindings {
  @override
  void dependencies() {
    //API BASE HELPER
    Get.put(
      ApiBaseHelper(
        client: GetConnect().httpClient,
        interceptors: [
          LoggerInterceptor(),
          if (AppFlavor.isDev) ...[
            Get.find<AliceGetConnect>(),
          ],
        ],
      ),
      permanent: true,
    );

    DataUserBinding().dependencies();
  }
}
