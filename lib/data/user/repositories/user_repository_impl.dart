import 'package:dartz/dartz.dart';
import 'package:maha_flutter_test/_core/extensions.dart';
import 'package:maha_flutter_test/data/_core/app_exceptions.dart';
import 'package:maha_flutter_test/data/user/datasources/user_remote_datasource.dart';
import 'package:maha_flutter_test/domain/_core/app_failures.dart';
import 'package:maha_flutter_test/domain/user/entities/user_list_entity.dart';
import 'package:maha_flutter_test/domain/user/repositories/user_repository.dart';

class UserRepositoryImpl implements UserRepository {
  final UserRemoteDatasource userRemoteDatasource;

  UserRepositoryImpl(this.userRemoteDatasource);

  @override
  Future<Either<AppFailure, UserListEntity>> retrieveUserList(int page) async {
    try {
      final users = await userRemoteDatasource.retrieveUserList(page);
      return Right(users.toEntity());
    } on AppException catch (e) {
      return Left(e.toAppFailure());
    } catch (e) {
      return const Left(AppFailure.general("Unknown Error"));
    }
  }

  @override
  Future<Either<AppFailure, String>> submitUser(String name, String job) async {
    try {
      await userRemoteDatasource.submitUser(name, job);
      return const Right('User Created');
    } on AppException catch (e) {
      return Left(e.toAppFailure());
    } catch (e) {
      return const Left(AppFailure.general("Unknown Error"));
    }
  }

  @override
  Future<Either<AppFailure, String>> deleteUser(int id) async {
    try {
      await userRemoteDatasource.deleteUser(id);
      return const Right('User Deleted');
    } on AppException catch (e) {
      return Left(e.toAppFailure());
    } catch (e) {
      return const Left(AppFailure.general("Unknown Error"));
    }
  }

  @override
  Future<Either<AppFailure, String>> updateUser(
      int id, String name, String job) async {
    try {
      await userRemoteDatasource.updateUser(id, name, job);
      return const Right('User Updated');
    } on AppException catch (e) {
      return Left(e.toAppFailure());
    } catch (e) {
      return const Left(AppFailure.general("Unknown Error"));
    }
  }
}
