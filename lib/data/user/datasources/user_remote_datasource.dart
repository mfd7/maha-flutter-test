import 'package:maha_flutter_test/data/_core/api_base_helper.dart';
import 'package:maha_flutter_test/data/user/models/user_list_model.dart';
import 'package:maha_flutter_test/data/user/user_endpoints.dart';

abstract class UserRemoteDatasource {
  Future<UserListModel> retrieveUserList(int page);
  Future<void> submitUser(String name, String job);
  Future<void> deleteUser(int id);
  Future<void> updateUser(int id, String name, String job);
}

class UserRemoteDatasourceImpl implements UserRemoteDatasource {
  final ApiBaseHelper apiBaseHelper;

  UserRemoteDatasourceImpl(this.apiBaseHelper);

  @override
  Future<UserListModel> retrieveUserList(int page) async {
    final responseBody =
        await apiBaseHelper.getApi(UserEndpoints.users, query: {
      'page': page.toString(),
      'per_page': '10',
    });
    return UserListModel.fromJson(responseBody);
  }

  @override
  Future<void> submitUser(String name, String job) async {
    await apiBaseHelper.postApi(UserEndpoints.users, {
      'name': name,
      'job': job,
    });
  }

  @override
  Future<void> deleteUser(int id) async {
    await apiBaseHelper.deleteApi('${UserEndpoints.users}/$id');
  }

  @override
  Future<void> updateUser(int id, String name, String job) async {
    await apiBaseHelper.putApi('${UserEndpoints.users}/$id', {
      'name': name,
      'job': job,
    });
  }
}
