import 'package:get/get.dart';
import 'package:maha_flutter_test/data/user/datasources/user_remote_datasource.dart';
import 'package:maha_flutter_test/data/user/repositories/user_repository_impl.dart';
import 'package:maha_flutter_test/domain/user/repositories/user_repository.dart';

class DataUserBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UserRemoteDatasource>(
      () => UserRemoteDatasourceImpl(Get.find()),
      fenix: true,
    );
    Get.lazyPut<UserRepository>(
      () => UserRepositoryImpl(Get.find()),
      fenix: true,
    );
  }
}
