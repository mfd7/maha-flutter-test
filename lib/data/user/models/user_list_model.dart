import 'package:maha_flutter_test/domain/_core/entities/meta.dart';
import 'package:maha_flutter_test/domain/user/entities/user_entity.dart';
import 'package:maha_flutter_test/domain/user/entities/user_list_entity.dart';

class UserListModel {
  UserListModel({
    num? page,
    num? perPage,
    num? total,
    num? totalPages,
    List<UserModel>? data,
    Support? support,
  }) {
    _page = page;
    _perPage = perPage;
    _total = total;
    _totalPages = totalPages;
    _data = data;
    _support = support;
  }

  UserListModel.fromJson(dynamic json) {
    _page = json['page'];
    _perPage = json['per_page'];
    _total = json['total'];
    _totalPages = json['total_pages'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(UserModel.fromJson(v));
      });
    }
    _support =
        json['support'] != null ? Support.fromJson(json['support']) : null;
  }
  num? _page;
  num? _perPage;
  num? _total;
  num? _totalPages;
  List<UserModel>? _data;
  Support? _support;

  num get page => _page ?? 0;
  num get perPage => _perPage ?? 0;
  num get total => _total ?? 0;
  num get totalPages => _totalPages ?? 0;
  List<UserModel> get users => _data ?? [];
  Support? get support => _support;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['page'] = _page;
    map['per_page'] = _perPage;
    map['total'] = _total;
    map['total_pages'] = _totalPages;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    if (_support != null) {
      map['support'] = _support?.toJson();
    }
    return map;
  }

  UserListEntity toEntity() {
    return UserListEntity(
      meta: Meta(
        page: page.toInt(),
        perPage: perPage.toInt(),
        total: total.toInt(),
        totalPages: totalPages.toInt(),
      ),
      users: users.map((e) => e.toEntity()).toList(),
    );
  }
}

class Support {
  Support({
    String? url,
    String? text,
  }) {
    _url = url;
    _text = text;
  }

  Support.fromJson(dynamic json) {
    _url = json['url'];
    _text = json['text'];
  }
  String? _url;
  String? _text;

  String? get url => _url;
  String? get text => _text;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['url'] = _url;
    map['text'] = _text;
    return map;
  }
}

class UserModel {
  UserModel({
    num? id,
    String? email,
    String? firstName,
    String? lastName,
    String? avatar,
  }) {
    _id = id;
    _email = email;
    _firstName = firstName;
    _lastName = lastName;
    _avatar = avatar;
  }

  UserModel.fromJson(dynamic json) {
    _id = json['id'];
    _email = json['email'];
    _firstName = json['first_name'];
    _lastName = json['last_name'];
    _avatar = json['avatar'];
  }
  num? _id;
  String? _email;
  String? _firstName;
  String? _lastName;
  String? _avatar;

  num get id => _id ?? 0;
  String get email => _email ?? '';
  String get firstName => _firstName ?? '';
  String get lastName => _lastName ?? '';
  String get avatar => _avatar ?? '';

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['email'] = _email;
    map['first_name'] = _firstName;
    map['last_name'] = _lastName;
    map['avatar'] = _avatar;
    return map;
  }

  UserEntity toEntity() {
    return UserEntity(
        id: id.toInt(),
        imageUrl: avatar,
        name: '$firstName $lastName',
        email: email);
  }
}
