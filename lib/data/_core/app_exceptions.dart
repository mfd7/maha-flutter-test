import 'package:freezed_annotation/freezed_annotation.dart';

part 'app_exceptions.freezed.dart';

@freezed
class AppException with _$AppException implements Exception {
  const factory AppException.network(int? statusCode) = network;

  const factory AppException.parsingData() = ParsingData;
}
