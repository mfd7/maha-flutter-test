import 'package:get/get.dart';
import 'package:maha_flutter_test/presentation/screens/edit_add/edit_add_binding.dart';
import 'package:maha_flutter_test/presentation/screens/edit_add/screens/edit_add_screen.dart';
import 'package:maha_flutter_test/presentation/screens/home/home_binding.dart';
import 'package:maha_flutter_test/presentation/screens/home/screens/home_screen.dart';
import 'package:maha_flutter_test/presentation/screens/splash/screens/splash_screen.dart';
import 'package:maha_flutter_test/presentation/screens/splash/splash_binding.dart';

class AppPage {
  static const splashScreen = '/';
  static const homeScreen = '/home';
  static const editAddScreen = '/edit-add';

  static var pages = <GetPage>[
    //SPLASH
    GetPage(
      name: splashScreen,
      page: () => const SplashScreen(),
      binding: SplashBinding(),
    ),
    //HOME
    GetPage(
      name: homeScreen,
      page: () => HomeScreen(),
      binding: HomeBinding(),
    ),
    //EDIT ADD
    GetPage(
      name: editAddScreen,
      page: () => EditAddScreen(),
      binding: EditAddBinding(),
    ),
  ];
}
