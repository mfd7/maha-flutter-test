class AppConstant {
  static const kAppName = 'Maha Flutter Test';
  static const kParsingDataCode = "parsing_data_exception";
  static const kConnectionError = "connection_timeout";

  static const kConnectionTimeout = 30000;
  static const kDefaultPadding = 16.0;
}
