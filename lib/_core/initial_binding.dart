import 'package:get/get.dart';
import 'package:maha_flutter_test/data/data_binding.dart';
import 'package:maha_flutter_test/domain/domain_binding.dart';
import 'package:maha_flutter_test/presentation/presentation_binding.dart';

class InitialBinding extends Bindings {
  @override
  void dependencies() {
    DataBinding().dependencies();
    DomainBinding().dependencies();
    PresentationBinding().dependencies();
  }
}
