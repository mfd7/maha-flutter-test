import 'package:flutter/material.dart';
import 'package:maha_flutter_test/_core/constants/app_flavor.dart';
import 'package:maha_flutter_test/_core/my_app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  AppFlavor(
    baseUrl: 'https://reqres.in',
    flavor: Flavor.dev,
  );
  runApp(const MyApp());
}
